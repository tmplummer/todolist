package com.plummer;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	
	@RequestMapping("/hi")
	public String printHello(ModelMap model) {
		model.put("message", "Hello Spring MVC Framework!");
		return "hello";
	}

}