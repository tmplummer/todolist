package com.plummer;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import com.plummer.clef.springmvccomponents.ClefApplicationCredentials;
import com.plummer.clef.springmvccomponents.ClefLoginConfigurer;

@Configuration
@EnableWebSecurity
public class MySecurity extends WebSecurityConfigurerAdapter {
	private static final String CLEF_APP_SECRET = "8ebed501510ae9d80d11497f48ff27f4";
	private static final String CLEF_APP_ID = "0283bdd4a75928ee7af30c5df1bd59e1";
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/clefLogout").permitAll();
		http.authorizeRequests().antMatchers("/resources/**").permitAll();
		http.authorizeRequests().anyRequest().authenticated();
		http.setSharedObject(UserDetailsService.class, new MyUserDetailsService());
		ClefLoginConfigurer<HttpSecurity> configurer = http.apply(new ClefLoginConfigurer<HttpSecurity>());
		configurer.clefApplicationCredentials(new ClefApplicationCredentials(CLEF_APP_ID, CLEF_APP_SECRET));
		configurer.logoutUrlForClef("/clefLogout");
		configurer.loginPage("/clefStart").permitAll();
	}
	
}
