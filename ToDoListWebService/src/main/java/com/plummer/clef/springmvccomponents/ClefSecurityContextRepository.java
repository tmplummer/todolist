package com.plummer.clef.springmvccomponents;

import org.springframework.security.web.context.SecurityContextRepository;

public interface ClefSecurityContextRepository extends SecurityContextRepository {

	public abstract void logoutUserWithClefId(long clefId);

}