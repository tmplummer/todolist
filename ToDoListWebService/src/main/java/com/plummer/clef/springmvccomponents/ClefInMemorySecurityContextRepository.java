package com.plummer.clef.springmvccomponents;

import java.util.ConcurrentModificationException;
import java.util.WeakHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

public class ClefInMemorySecurityContextRepository extends HttpSessionSecurityContextRepository implements ClefSecurityContextRepository {

	private WeakHashMap<SecurityContext, Object> weakSecurityContexts = new WeakHashMap<SecurityContext, Object>();
	
	@Override
	public void saveContext(SecurityContext context, HttpServletRequest request, HttpServletResponse response) {
		super.saveContext(context, request, response);
		weakSecurityContexts.put(context, null);
	}

	@Override
	public void logoutUserWithClefId(long clefId) {
		while (true) {
			try {
				for (SecurityContext securityContext : weakSecurityContexts.keySet()) {
					if (securityContext.getAuthentication() instanceof ClefAuthenticationToken) {
						ClefAuthenticationToken authToken = (ClefAuthenticationToken) securityContext.getAuthentication();
						if (authToken.getClefUserInfo().getId() == clefId) {
							authToken.performClefLogout();
						}
					}
				}
				return;
			} catch (ConcurrentModificationException e) {
				
			}
		}
	}
	
}
