package com.plummer.clef.springmvccomponents;

import org.springframework.util.LinkedMultiValueMap;

public class ClefApplicationCredentials {

	private final String clefAppId;
	private final String clefAppSecret;
	
	public ClefApplicationCredentials(String clefAppId, String clefAppSecret) {
		this.clefAppId = clefAppId;
		this.clefAppSecret = clefAppSecret;
	}

	public String getClefAppId() {
		return clefAppId;
	}

	public String getClefAppSecret() {
		return clefAppSecret;
	}
	
	public LinkedMultiValueMap<String, String> toAuthorizeRequestParameters(String code) {
		LinkedMultiValueMap<String, String> authorizeRequestParameters = toRequestParameters();
		authorizeRequestParameters.add("code", code);
		return authorizeRequestParameters;
	}
	
	public LinkedMultiValueMap<String, String> toLogoutRequestParameters(String logoutToken) {
		LinkedMultiValueMap<String, String> authorizeRequestParameters = toRequestParameters();
		authorizeRequestParameters.add("logout_token", logoutToken);
		return authorizeRequestParameters;
	}

	private LinkedMultiValueMap<String, String> toRequestParameters() {
		LinkedMultiValueMap<String, String> authorizeRequestParameters = new LinkedMultiValueMap<String, String>();
		authorizeRequestParameters.add("app_id", clefAppId);
		authorizeRequestParameters.add("app_secret", clefAppSecret);
		return authorizeRequestParameters;
	}
	
}
