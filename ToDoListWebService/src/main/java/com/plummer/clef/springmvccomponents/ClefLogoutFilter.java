package com.plummer.clef.springmvccomponents;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import com.plummer.clef.clefApi.ClefApi;
import com.plummer.clef.clefApi.domain.ClefLogoutResponse;

public class ClefLogoutFilter extends GenericFilterBean {
	private static final String LOGOUT_TOKEN_REQUEST_PARAMETER = "logout_token";
	private RequestMatcher clefLogUserOutRequestMatcher = new AntPathRequestMatcher("/clefLogout");
	private ClefApplicationCredentials clefApplicationCredentials;
	private ClefSecurityContextRepository clefSecurityContextRepository;
	private ClefApi clefApi = new ClefApi();

	@Override
	public void afterPropertiesSet() throws ServletException {
		super.afterPropertiesSet();
		Assert.notNull(clefApplicationCredentials, "The clefApplicationCredentials must be set");
		Assert.notNull(clefSecurityContextRepository, "The clefSecurityContextRepository must be set");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		if (requiresUserToBeLoggedOut(request)) {
			logOutRequestedUser(request);
		} else {
			chain.doFilter(request, response);
		}
	}

	private void logOutRequestedUser(ServletRequest request) {
		String logoutToken = request.getParameter(LOGOUT_TOKEN_REQUEST_PARAMETER);
		ClefLogoutResponse logoutResponse = clefApi.logout(clefApplicationCredentials, logoutToken);
		clefSecurityContextRepository.logoutUserWithClefId(logoutResponse.getClefId());
	}

	protected boolean requiresUserToBeLoggedOut(ServletRequest request) {
		return clefLogUserOutRequestMatcher.matches((HttpServletRequest) request);
	}

	public void setClefLogUserOutRequestMatcher(RequestMatcher clefLogUserOutRequestMatcher) {
		Assert.notNull(clefLogUserOutRequestMatcher, "clefLogUserOutRequestMatcher must not be null");
		this.clefLogUserOutRequestMatcher = clefLogUserOutRequestMatcher;
	}

	public void setClefApplicationCredentials(ClefApplicationCredentials clefApplicationCredentials) {
		Assert.notNull(clefLogUserOutRequestMatcher, "clefApplicationCredentials must not be null");
		this.clefApplicationCredentials = clefApplicationCredentials;
	}

	public void setClefSecurityContextRepository(ClefSecurityContextRepository clefSecurityContextRepository) {
		this.clefSecurityContextRepository = clefSecurityContextRepository;
	}

}
