package com.plummer.clef.springmvccomponents;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.plummer.clef.clefApi.domain.ClefUserInfo;

public class ClefAuthenticationToken extends AbstractAuthenticationToken {
	private static final long serialVersionUID = 4620641777232780606L;
	private ClefUserInfo info;
	private final Object principal;
	private boolean loggedOutByClef = false;

	public ClefAuthenticationToken(Object principal, ClefUserInfo info, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.info = info;
		this.principal = principal;
		setAuthenticated(true);
	}
	
	public ClefAuthenticationToken(ClefUserInfo info) {
		this(info.getId(), info, null);
	}
	
	public ClefUserInfo getClefUserInfo() {
		return info;
	}

	@Override
	public Object getCredentials() {
		return null;
	}

	@Override
	public Object getPrincipal() {
		return principal;
	}

	public boolean loggedOutFromClef() {
		return loggedOutByClef;
	}
	
	public void performClefLogout() {
		setAuthenticated(false);
		loggedOutByClef = true;
	}

}
