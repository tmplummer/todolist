package com.plummer.clef.springmvccomponents;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.Assert;

public class ClefAuthenticationProvider implements AuthenticationProvider, InitializingBean {

	private AuthenticationUserDetailsService<ClefAuthenticationToken> userDetailsService;
	private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(this.userDetailsService, "The userDetailsService must be set");
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		if (!supports(authentication.getClass())) {
			return null;
		}

		if (authentication instanceof ClefAuthenticationToken) {
			ClefAuthenticationToken response = (ClefAuthenticationToken) authentication;
			if (response.loggedOutFromClef()) {
				throw new CredentialsExpiredException("User logged out of clef");
			}
			UserDetails userDetails = userDetailsService.loadUserDetails(response);
			return createSuccessfulAuthentication(userDetails, response);
		}

		return null;
	}

	/**
	 * Handles the creation of the final <tt>Authentication</tt> object which
	 * will be returned by the provider.
	 * <p>
	 * The default implementation just creates a new ClefAuthenticationToken
	 * from the original, but with the UserDetails as the principal and
	 * including the authorities loaded by the UserDetailsService.
	 *
	 * @param userDetails
	 *            the loaded UserDetails object
	 * @param auth
	 *            the token passed to the authenticate method, containing
	 * @return the token which will represent the authenticated user.
	 */
	protected Authentication createSuccessfulAuthentication(UserDetails userDetails, ClefAuthenticationToken auth) {
		return new ClefAuthenticationToken(userDetails, auth.getClefUserInfo(), authoritiesMapper.mapAuthorities(userDetails.getAuthorities()));
	}

	/**
	 * Used to load the {@code UserDetails} for the authenticated Clef user.
	 */
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = new UserDetailsByNameServiceWrapper<ClefAuthenticationToken>(userDetailsService);
	}

	/**
	 * Used to load the {@code UserDetails} for the authenticated Clef user.
	 */
	public void setAuthenticationUserDetailsService(AuthenticationUserDetailsService<ClefAuthenticationToken> userDetailsService) {
		this.userDetailsService = userDetailsService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.authentication.AuthenticationProvider#supports
	 * (java.lang.Class)
	 */
	public boolean supports(Class<?> authentication) {
		return ClefAuthenticationToken.class.isAssignableFrom(authentication);
	}

}
