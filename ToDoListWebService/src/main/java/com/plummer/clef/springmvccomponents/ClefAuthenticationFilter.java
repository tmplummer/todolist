package com.plummer.clef.springmvccomponents;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.Assert;

import com.plummer.clef.clefApi.ClefApi;
import com.plummer.clef.clefApi.domain.ClefAuthorizeResponse;
import com.plummer.clef.clefApi.domain.ClefInfoResponse;

public class ClefAuthenticationFilter extends AbstractAuthenticationProcessingFilter implements InitializingBean {
	private ClefApplicationCredentials clefApplicationCredentials;
	private ClefApi clefApi = new ClefApi();

	public ClefAuthenticationFilter() {
		super(new AntPathRequestMatcher("/clefLogin"));
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Assert.notNull(clefApplicationCredentials, "The clefApplicationCredentials must be set");
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException,
			ServletException {
		String clefCode = request.getParameter("code");
		ClefAuthorizeResponse authorizeResponse = clefApi.authorize(clefApplicationCredentials, clefCode);
		if (authorizeResponse.isSuccess()) {
			ClefInfoResponse infoResponse = clefApi.info(authorizeResponse);
			if (infoResponse.isSuccess()) {
				return getAuthenticationManager().authenticate(new ClefAuthenticationToken(infoResponse.getInfo()));
			} else {
				throw new AuthenticationCredentialsNotFoundException("Error from Clef: " + infoResponse.getError());
			}
		}
		throw new AuthenticationCredentialsNotFoundException("Error from Clef: " + authorizeResponse.getError());
	}

	public void setClefApplicationCredentials(ClefApplicationCredentials clefApplicationCredentials) {
		this.clefApplicationCredentials = clefApplicationCredentials;
	}

}
