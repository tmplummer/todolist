package com.plummer.clef.springmvccomponents;

import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractAuthenticationFilterConfigurer;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class ClefLoginConfigurer<H extends HttpSecurityBuilder<H>> extends
		AbstractAuthenticationFilterConfigurer<H, ClefLoginConfigurer<H>, ClefAuthenticationFilter> {
	private AuthenticationUserDetailsService<ClefAuthenticationToken> authenticationUserDetailsService;
	private ClefApplicationCredentials clefApplicationCredentials;
	private ClefSecurityContextRepository clefSecurityContextRepository = new ClefInMemorySecurityContextRepository();
	private String logoutUrlForClef;

	public ClefLoginConfigurer() {
		super(new ClefAuthenticationFilter(), "/clefLogin");
	}

	@Override
	public void init(H http) throws Exception {
		super.init(http);
		ClefAuthenticationFilter authenticationFilter = getAuthenticationFilter();
		authenticationFilter.setClefApplicationCredentials(clefApplicationCredentials);
		http.addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class);
		
		ClefAuthenticationProvider authenticationProvider = new ClefAuthenticationProvider();
		authenticationProvider.setAuthenticationUserDetailsService(getAuthenticationUserDetailsService(http));
		authenticationProvider = postProcess(authenticationProvider);
		http.authenticationProvider(authenticationProvider);
		if (logoutUrlForClef != null) {
			((HttpSecurity) http).securityContext().securityContextRepository(clefSecurityContextRepository);
			ClefLogoutFilter clefLogoutFilter = new ClefLogoutFilter();
			clefLogoutFilter.setClefLogUserOutRequestMatcher(new AntPathRequestMatcher(logoutUrlForClef));
			clefLogoutFilter.setClefApplicationCredentials(clefApplicationCredentials);
			clefLogoutFilter.setClefSecurityContextRepository(clefSecurityContextRepository);
			http.addFilterBefore(clefLogoutFilter, LogoutFilter.class);
			http.addFilter(postProcess(clefLogoutFilter));
		}
	}

	public ClefLoginConfigurer<H> logoutUrlForClef(String logoutUrlForClef) {
		this.logoutUrlForClef = logoutUrlForClef;
		return this;
	}

	@Override
	protected RequestMatcher createLoginProcessingUrlMatcher(String loginProcessingUrl) {
		return new AntPathRequestMatcher(loginProcessingUrl);
	}

	@Override
	public ClefLoginConfigurer<H> loginPage(String loginPage) {
		return super.loginPage(loginPage);
	}

	/**
	 * The {@link AuthenticationUserDetailsService} to use. By default a
	 * {@link UserDetailsByNameServiceWrapper} is used with the
	 * {@link UserDetailsService} shared object found with
	 * {@link HttpSecurity#getSharedObject(Class)}.
	 *
	 * @param authenticationUserDetailsService
	 *            the {@link AuthenticationDetailsSource} to use
	 * @return the {@link ClefLoginConfigurer} for further customizations
	 */
	public ClefLoginConfigurer<H> authenticationUserDetailsService(AuthenticationUserDetailsService<ClefAuthenticationToken> authenticationUserDetailsService) {
		this.authenticationUserDetailsService = authenticationUserDetailsService;
		return this;
	}

	/**
	 * Gets the {@link AuthenticationUserDetailsService} that was configured or
	 * defaults to {@link UserDetailsByNameServiceWrapper} that uses a
	 * {@link UserDetailsService} looked up using
	 * {@link HttpSecurity#getSharedObject(Class)}
	 *
	 * @param http
	 *            the current {@link HttpSecurity}
	 * @return the {@link AuthenticationUserDetailsService}.
	 */
	private AuthenticationUserDetailsService<ClefAuthenticationToken> getAuthenticationUserDetailsService(H http) {
		if (authenticationUserDetailsService != null) {
			return authenticationUserDetailsService;
		}
		return new UserDetailsByNameServiceWrapper<ClefAuthenticationToken>(http.getSharedObject(UserDetailsService.class));
	}

	public void clefApplicationCredentials(ClefApplicationCredentials clefApplicationCredentials) {
		this.clefApplicationCredentials = clefApplicationCredentials;
	}

	public void clefSecurityContextRepository(ClefSecurityContextRepository clefSecurityContextRepository) {
		this.clefSecurityContextRepository = clefSecurityContextRepository;
	}

}
