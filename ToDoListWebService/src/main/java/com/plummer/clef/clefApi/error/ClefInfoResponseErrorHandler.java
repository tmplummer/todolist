package com.plummer.clef.clefApi.error;

import java.io.IOException;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

public class ClefInfoResponseErrorHandler extends DefaultResponseErrorHandler {
	
	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		return ! (response.getStatusCode() == HttpStatus.OK || response.getStatusCode() == HttpStatus.FORBIDDEN);
	}

}
