package com.plummer.clef.clefApi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClefAuthorizeResponse {
	@JsonProperty("access_token")
	private String accessToken;
	private boolean success;
	private String error;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "accessToken={" + accessToken + "},success={" + success + "}";
	}

}
