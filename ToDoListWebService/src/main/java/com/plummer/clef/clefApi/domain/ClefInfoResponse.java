package com.plummer.clef.clefApi.domain;


public class ClefInfoResponse {

	private ClefUserInfo info;
	private boolean success;
	private String error;

	public ClefUserInfo getInfo() {
		return info;
	}

	public void setInfo(ClefUserInfo info) {
		this.info = info;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "ClefInfoResponse [info=" + info + ", success=" + success + "]";
	}

	
}
