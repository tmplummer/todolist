package com.plummer.clef.clefApi;

import org.springframework.web.client.RestTemplate;

import com.plummer.clef.clefApi.domain.ClefAuthorizeResponse;
import com.plummer.clef.clefApi.domain.ClefInfoResponse;
import com.plummer.clef.clefApi.domain.ClefLogoutResponse;
import com.plummer.clef.clefApi.error.ClefAuthorizeResponseErrorHandler;
import com.plummer.clef.clefApi.error.ClefInfoResponseErrorHandler;
import com.plummer.clef.clefApi.error.ClefLogoutResponseErrorHandler;
import com.plummer.clef.springmvccomponents.ClefApplicationCredentials;

public class ClefApi {
	private static String CLEF_AUTHORIZE_URL = "https://clef.io/api/v1/authorize";
	private static String CLEF_INFO_URL = "https://clef.io/api/v1/info?access_token={access_token}";
	private static String CLEF_LOGOUT_URL = "https://clef.io/api/v1/logout";
	private RestTemplate restTemplate = new RestTemplate();
	
	public ClefAuthorizeResponse authorize(ClefApplicationCredentials clefApplicationCredentials, String clefCode) {
		restTemplate.setErrorHandler(new ClefAuthorizeResponseErrorHandler());
		return restTemplate.postForObject(CLEF_AUTHORIZE_URL, clefApplicationCredentials.toAuthorizeRequestParameters(clefCode),
				ClefAuthorizeResponse.class);
	}
	
	public ClefInfoResponse info(ClefAuthorizeResponse authorizeResponse) {
		restTemplate.setErrorHandler(new ClefInfoResponseErrorHandler());
		return restTemplate.getForObject(CLEF_INFO_URL, ClefInfoResponse.class, authorizeResponse.getAccessToken());
	}
	
	public ClefLogoutResponse logout(ClefApplicationCredentials clefApplicationCredentials, String logoutToken) {
		restTemplate.setErrorHandler(new ClefLogoutResponseErrorHandler());
		return restTemplate.postForObject(CLEF_LOGOUT_URL, clefApplicationCredentials.toLogoutRequestParameters(logoutToken),
				ClefLogoutResponse.class);
	}
}
