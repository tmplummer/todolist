package com.plummer.clef.clefApi.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClefLogoutResponse {
	@JsonProperty("clef_id")
	private long clefId;
	private boolean success;
	private String error;

	public long getClefId() {
		return clefId;
	}

	public void setClefId(long clefId) {
		this.clefId = clefId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "ClefLogoutResponse [clefId=" + clefId + ", success=" + success
				+ "]";
	}

}
