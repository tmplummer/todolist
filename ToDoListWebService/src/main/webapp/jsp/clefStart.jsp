<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Bootstrap 101 Template</title>
<link href="resources/bootstrap.min.css" rel="stylesheet">
<script src="resources/jquery-2.1.1.min.js"></script>
<script src="resources/bootstrap.min.js"></script>
</head>
<body>
	<h1>Hello, world!</h1>

	<ul class="list-group">
		<li class="list-group-item">Cras justo odio
			<ul class="list-group">
				<li class="list-group-item">sub1</li>
				<li class="list-group-item">sub2</li>
			</ul>
		</li>
		<li class="list-group-item">Dapibus ac facilisis in</li>
		<li class="list-group-item">Morbi leo risus</li>
		<li class="list-group-item">Porta ac consectetur ac</li>
		<li class="list-group-item">Vestibulum at eros</li>
	</ul>

	<script type="text/javascript" src="https://clef.io/v3/clef.js"
		class="clef-button" data-app-id="0283bdd4a75928ee7af30c5df1bd59e1"
		data-color="blue" data-style="flat"
		data-redirect-url="http://localhost:8080/clefLogin" data-type="login"></script>
</body>
</html>